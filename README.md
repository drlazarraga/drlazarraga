¿Qué son los ácidos grasos omega-3?
Los omega-3 son nutrientes que se obtienen de los alimentos (o de los suplementos) y que ayudan a construir y mantener un cuerpo sano. Son fundamentales para la estructura de todas las paredes celulares que tienes. También son una fuente de energía y ayudan a que el corazón, los pulmones, los vasos sanguíneos y el sistema inmunitario funcionen como deben.

Dos de ellos, el EPA y el DHA, se encuentran principalmente en ciertos pescados. El ALA (ácido alfa-linolénico), otro ácido graso omega-3, se encuentra en fuentes vegetales como los frutos secos y las semillas.

Los niveles de DHA son especialmente altos en la retina (ojo), el cerebro y los espermatozoides.

El cuerpo no sólo necesita estos ácidos grasos para funcionar, sino que también aportan grandes beneficios para la salud.

Beneficios para la salud de los ácidos grasos Omega-3
Grasa en sangre (triglicéridos). El aceite de pescado puede reducir los niveles elevados de triglicéridos. Tener niveles elevados de esta grasa en sangre supone un riesgo de padecer enfermedades cardíacas y accidentes cerebrovasculares.

Artritis reumatoide. Los suplementos de aceite de pescado (EPA+DHA) pueden frenar la rigidez y el dolor articular. Los suplementos de omega-3 también parecen potenciar la eficacia de los medicamentos antiinflamatorios.

Depresión. Algunos investigadores han descubierto que las culturas que consumen alimentos con altos niveles de omega-3 tienen niveles más bajos de depresión. Los efectos de los suplementos de aceite de pescado sobre la depresión han sido contradictorios. Se necesita más investigación para ver si puede marcar la diferencia.

Desarrollo del bebé. El DHA parece ser importante para el desarrollo visual y neurológico de los bebés.

Asma. Una dieta rica en omega-3 reduce la inflamación, un componente clave del asma. Pero se necesitan más estudios para demostrar si los suplementos de aceite de pescado mejoran la función pulmonar o reducen la cantidad de medicación que una persona necesita para controlar la enfermedad.


https://drlazarraga.com/producto/complejo-omega-3/